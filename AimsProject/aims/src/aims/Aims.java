package aims;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import aims.disc.DigitalVideoDisc;
import aims.media.Media;
import aims.order.Order;

public class Aims {
    static ArrayList<Media> items = new ArrayList<Media>();

    public static void main(String[] args) throws Exception {
        Order anOrder = Order.createOrder();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addMedia(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star wars");
        dvd2.setCategory("Science fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addMedia(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(87);
        anOrder.addMedia(dvd3);

        String title = "King lion";
        System.out.println(title + " is :" + dvd1.search(title));
        Media luckyItem = anOrder.getALuckyItem();
        // System.out.println(anOrder.totalCost());
        // anOrder.removeDigitalVideoDisc(dvd3);
        System.out.println("Lucky Item is: " + luckyItem.getTitle());
        // System.out.println(anOrder.totalCost());

        Scanner scanner = new Scanner(System.in);
        items.add(dvd1);
        items.add(dvd2);
        items.add(dvd3);

        showMenu(scanner);
        scanner.close();

    }

    static Order newOrder = null;

    public static void showMenu(Scanner scanner) {
        System.out.println("Order Management Application: ");
        System.out.println("-------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add Item to the order");
        System.out.println("3. Delete Item by Id");
        System.out.println("4. Display the item list of orders");
        System.out.println("0. Exit");
        System.out.println("Please choose a number: 0-1-2-3-4: ");
        int choice = Integer.parseInt(scanner.nextLine());
        switch (choice) {
            case 1:
                newOrder = Order.createOrder();
                System.out.println("Order's created successfully");
                break;
            case 2:
                if (newOrder == null) {
                    System.out.println("Create an Order first");
                    break;
                }
                System.out.println("Enter the id: ");
                int id = Integer.parseInt(scanner.nextLine());
                addItemById(newOrder, id);
                break;
            case 3:
                if (newOrder == null) {
                    System.out.println("Create an Order first");
                    break;
                }
                System.out.println("Enter the id: ");
                id = Integer.parseInt(scanner.nextLine());
                deleteItemById(newOrder, id);
                break;
            case 4:
                if (newOrder == null) {
                    System.out.println("Create an Order first");
                    break;
                }
                printAllOrderList(newOrder);
                break;
            case 0:
                break;
            default:
                System.out.println("Invalid action, try again");
        }
        if (choice != 0)
            showMenu(scanner);
        return;
    }

    static void addItemById(Order order, int id) {
        int selectedIndex = -1;
        for (int i = 0; i < items.size(); i++) {
            if (id == items.get(i).getId()) {
                selectedIndex = i;
                break;
            }
        }
        if (selectedIndex > -1)
            order.addMedia(items.get(selectedIndex));
        else
            System.out.println("No such item");

    }

    static void deleteItemById(Order order, int id) {
        int index = -1;
        for (int i = 0; i < order.getItemsOrdered().size(); i++) {
            if (id == order.getItemsOrdered().get(i).getId()) {
                index = i;
                break;
            }
        }
        if (index < 0) {
            System.out.println("No such item");
            return;
        }

        order.removeMedia(order.getItemsOrdered().get(index));
    }

    static void printAllOrderList(Order order) {
        for (int i = 0; i < order.getItemsOrdered().size(); i++) {
            Media item = order.getItemsOrdered().get(i);
            System.out.println(item.getId() + ": " + item.getTitle() + " - $" + item.getCost());
        }
    }
}
