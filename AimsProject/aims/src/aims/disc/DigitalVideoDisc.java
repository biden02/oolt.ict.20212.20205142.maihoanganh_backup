package aims.disc;

import aims.media.Media;

public class DigitalVideoDisc extends Media {

    private String director;
    private int length;

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String category, String title) {
        super(title, category);
    }

    public DigitalVideoDisc(String director, String category, String title) {
        super(title, category);
        this.director = director;
    }

    public DigitalVideoDisc(String director, String category, String title, int length, float cost) {
        super(title, category);
        this.director = director;
        this.setCost(cost);
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean search(String title) {
        boolean contain = false;
        String[] tokenArr = title.toLowerCase().split(" ", -1);
        for (String string : tokenArr) {
            if (this.getTitle().toLowerCase().contains(string))
                contain = true;
            else {
                return false;
            }
        }
        return contain;
    }

}