package aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();

    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title, category);
    }

    public Book(String title, String category, List<String> authors) {
        super(title, category);
        this.authors = authors;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    // int findAuthor(String authorName) {
    // int index = -1;
    // authors.forEach(n -> {
    // if(n.equals(anObject))
    // });
    // }

    public void addAuthor(String authorName) {
        if (authors.contains(authorName)) {
            System.out.println("Author existed");
            return;
        }
        authors.add(authorName);
    }

    public void removeAuthor(String authorName) {
        if (!authors.contains(authorName)) {
            System.out.println("Author not existed");
            return;
        }
        authors.remove(authorName);
    }
}
