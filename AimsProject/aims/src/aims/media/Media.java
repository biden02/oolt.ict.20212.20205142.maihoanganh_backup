package aims.media;

public class Media {
    private String title;
    private String category;
    private float cost;
    private static int prevId = -1;
    private int id;

    private static void genNewId() {
        prevId = prevId + 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Media() {
        this.id = prevId + 1;
        genNewId();
    }

    public Media(String title) {
        this();
        this.title = title;
    }

    public Media(String title, String category) {
        this(title);
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

}
