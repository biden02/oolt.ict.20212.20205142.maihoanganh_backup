import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Equations {
    public static void main(String[] args) {
        menu();
    }

    static void menu() {
        System.out.println("1. First degeree equation");
        System.out.println("2. System of First degeree equation");
        System.out.println("3. Second degeree equation");
        System.out.println("4. Exit\n");

        System.out.print("Please enter your choice: ");

        Scanner scanner = new Scanner(System.in);
        System.out.println();
        String option = scanner.nextLine();
        int optionNum = Integer.parseInt(option);
        int status = 1;
        switch (optionNum) {
            case 1:
                firstDegree();
                break;
            case 2:
                systemOfFirstDegree();
                break;
            case 3:
                secondDegree();
                break;
            case 4:
                status = 0;
                break;
            default:
                System.out.println("Invalid option! \n Try again");
        }
        if (status > 0) {
            menu();
        }

    }

    static void firstDegree() {
        JFrame jf = new JFrame();
        jf.setAlwaysOnTop(true);
        System.out.println("The equation has the form of: ax + b = 0");
        Scanner scanner = new Scanner(System.in);
        double a;
        double b;
        double x;
        System.out.println("Enter a: ");
        a = Double.parseDouble(scanner.nextLine());
        System.out.println("Enter b: ");
        b = Double.parseDouble(scanner.nextLine());
        x = -b / a;
        JOptionPane.showMessageDialog(jf, "The solution is x = " + x, "First degree equation",
                JOptionPane.INFORMATION_MESSAGE);
    }

    static void systemOfFirstDegree() {
        JFrame jf = new JFrame();
        jf.setAlwaysOnTop(true);
        System.out.println("The equation has the form of: a11x1 + a12x2 = b1 && a21x1 + a22x2 = b2");
        Scanner scanner = new Scanner(System.in);
        double a11;
        double a12;
        double b1;
        double a21;
        double a22;
        double b2;
        System.out.println("Enter a11, a12, b1, a21, a22, b2 seperated by new line: ");
        a11 = scanner.nextDouble();
        a12 = scanner.nextDouble();
        b1 = scanner.nextDouble();
        a21 = scanner.nextDouble();
        a22 = scanner.nextDouble();
        b2 = scanner.nextDouble();

        double D = a11 * a22 - a21 * a12;
        double D1 = b1 * a22 - b2 * a12;
        double D2 = a11 * b2 - a21 * b1;
        double x1;
        double x2;
        String outputTitle = "System of First degree equation";
        if (D != 0) {
            x1 = D1 / D;
            x2 = D2 / D;
            JOptionPane.showMessageDialog(jf, "The solution is x1 = " + x1 + " and x2 = " + x2,
                    outputTitle,
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (D1 == 0 && D2 == 0) {
            JOptionPane.showMessageDialog(jf, "The equation has infinite solutions",
                    outputTitle,
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(jf, "The equation has no solution", outputTitle,
                JOptionPane.INFORMATION_MESSAGE);
    }

    static void secondDegree() {
        JFrame jf = new JFrame();
        jf.setAlwaysOnTop(true);
        System.out.println("The equation has the form of: ax^2 + bx + c = 0");
        Scanner scanner = new Scanner(System.in);
        double a;
        double b;
        double c;
        String outputTitle = "Second degree equation";
        System.out.println("Enter a, b, c seperated by new line: ");
        a = scanner.nextDouble();
        b = scanner.nextDouble();
        c = scanner.nextDouble();
        double delta = b * b - 4 * a * c;
        if (delta == 0) {
            double x = -b / (2 * a);

            JOptionPane.showMessageDialog(jf, "The equation has 1 solution x = " + x, outputTitle,
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        if (delta > 0) {
            double x1 = (-b + Math.sqrt(delta)) / (2 * a);
            double x2 = (-b - Math.sqrt(delta)) / (2 * a);
            JOptionPane.showMessageDialog(jf, "The equation has 2 solutions x1 = " + x1 + " and x2 = " + x2,
                    outputTitle,
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(jf, "The equation has no solution",
                outputTitle,
                JOptionPane.INFORMATION_MESSAGE);
    }
}