package garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GarbageCreator {
    public static String readFile() {
        try {
            File file = new File(".");
            for (String fileNames : file.list())
                System.out.println(fileNames);
            BufferedReader br = new BufferedReader(new FileReader("file.txt"));

            String sb = "";
            String line = br.readLine();

            while (line != null) {
                sb += (line);
                sb += System.lineSeparator();
                line = br.readLine();
            }
            String everything = sb.toString();
            br.close();
            return everything;
        } catch (IOException err) {
            err.printStackTrace();
            return "Fail!";
        }
    }

    private GarbageCreator() {
    }
}
