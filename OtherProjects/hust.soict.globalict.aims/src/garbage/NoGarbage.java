package garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class NoGarbage {
    public static String readFile() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("file.txt"));

            StringBuilder sb = new StringBuilder();
            ;
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
            br.close();
            return everything;
        } catch (IOException err) {
            err.printStackTrace();
            return "Fail!";
        }
    }

    private NoGarbage() {
    }
}
